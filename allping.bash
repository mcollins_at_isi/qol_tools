#!/usr/bin/env bash
#
# Sequentially pings each of the hosts in the system so that you have all the
# key exchanges done
IFS=$'\n'
user_id=${1}
host_file=${2}
ip_file=${3}
for host in `cat ${host_file}`
do
    for ip in `cat ${ip_file}`
    do
        result=`ssh ${user_id}@${host} "ping -q -c 2 ${ip} | grep loss"`
        printf "%-20s %15s %s\n" ${host} ${ip} ${result}
    done
done