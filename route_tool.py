#!/usr/bin/env python
#
#
# given a database of routes, implement them.
import sys
import string
import os

ssh_id = ''

action = sys.argv[1]
route_db = sys.argv[2]

def gen_add_command(netblock, gateway, dev):
    """
    gen_add_string (netblock, gateway, dev) -> string

    Generates the sudo command for adding a route via ip route; the output an be fed
    directly into an ssh command.
    """
    result = 'sudo ip route add %s via %s' % (netblock, gateway)
    if dev is not None:
        result += ' dev %s' % dev
    return result

def gen_del_command(netblock):
    """
    gen_del_command (netblock) -> string

    Generates the sudo command for deleting a route via ip route; the output can
    be fed directly into an ssh command.
    """
    result = 'sudo ip route del %s' % netblock
    return result

def gen_list_command():
    """
    gen_list_command -> string

    Generates the sudo command for listing all the routes on an asset; the output
    can be fed driectly into ssh.
    """
    result = 'sudo ip route show'
    return result
    
def run_command(command, ssh_id, asset):
    """
    runs an ssh command to a remote host and captures the result in a pipe.
    """
    s = os.popen("ssh %s@%s '%s'" % (ssh_id, asset, command), 'r')

def parse_routeline(routeline):
    s = routeline.split() # We're using simple CSV for now
    asset = s[0]
    network = s[1]
    gateway = s[2]
    if len(s) > 3:
        device = s[3]
    else:
        device = None
    return asset, (network, gateway, device)

def read_routedb(routedb_fn):
    # Creates a table of routes by IP address
    # routes are in the format:
    # Asset name, subnetwork, gateway, device (optional)
    result = {}
    with open(routedb_fn,'r') as routedb_fp:
        for line in routedb_fp.readlines():
            asset, config = parse_routeline(line)
            if not asset in result:
                result[asset] = [config]
            else:
                result[asset].append(config)
    return result

def list_routes(asset, routedb):
    if asset in routedb:
        for network, gateway, device in routedb[asset]:
            print "%20s %20s %20s %20s" %(asset, network, gateway, device if device is not None else "")

def add_routes(asset, routedb, ssh_id):
    if asset in routedb:
        for network, gateway, device in routedb[asset]:
            print "ssh %s@%s '%s'" % (ssh_id, asset, gen_add_command(network, gateway, device))

def delete_routes(asset, routedb, ssh_id):
    if asset in routedb:
        for network, gateway, device in routedb[asset]:
            print "ssh %s@%s '%s'" % (ssh_id, asset, gen_del_command(network))

if __name__ == '__main__':
    command = sys.argv[1]
    routedb_fn = sys.argv[2]
    routedb = read_routedb(routedb_fn)
    if len(sys.argv) > 3:
        arguments = sys.argv[3:]
    else:
        arguments = ['all']

    # Now we check to see if the arguments are 'all'; if so, we substitute with
    # a list of the keys in the database, reducing to a previously solved condiction
    # Note thst this has a side effect that a list of say, 'all x' will expand to everything
    if 'all' in set(arguments):
        arguments = routedb.keys()
    arguments = sorted(arguments) # Final step
    if command == 'list':
        for i in arguments:
            list_routes(i, routedb)
    elif command == 'add':
        for i in arguments:
            add_routes(i, routedb, ssh_id)
    elif command == 'del':
        for i in arguments:
            delete_routes(i, routedb, ssh_id)
