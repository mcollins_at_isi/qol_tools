import mergexp as mx
classes = set(['rtr', 'node'])

class IPAddr:

    def int_ip_to_str(ip_address):
        """
        int_ip_to_str(subject [int]):
        Converts an ip address into a dotted quad string
        representation.  Assumes that the address is valid.
        """
        
        o1 = (ip_address >> 24) & 0x000000FF
        o2 = (ip_address >> 16) & 0x000000FF
        o3 = (ip_address >> 8) & 0x000000FF
        o4 = ip_address & 0x000000FF
        return "%d.%d.%d.%d" % (o1, o2, o3, o4)

    def str_ip_to_int(subject):
        """
        string_ip_to_int(subject)
        Converts a valid string representation as dotted quads
        to the integer equivalent.
        """
        if IPAddr.validate_str_address(subject):
            values = list(map(int, subject.split('.')))
            result = (values[0] << 24) + (values[1] << 16) + (values[2] << 8) \
                + values[3]
            return result
        else:
            return None
        
    def validate_int_address(subject):
        """
        Checks an integer IP address is in range (0, 2^32).  True good, false
        bad.
        """
        if subject >= 0 and subject <= 0xFFFFFFFF:
            return True
        else:
            return False

    def validate_str_address(subject):
        """
        Checks a stirng address to see if its acceptable, four integers
        between 0 and 256.  True good, False bad.
        """

        values = subject.split('.')
        # First check -- four dot separated elements
        if len(values) != 4:
            return False
        
        # Second check -- all values are integer
        try:
            values = map(int, values)
        except:
            return False
        
        # Third check -- all values are in [0,255]
        if len(list(filter(lambda x:(x >= 0 and x <= 255), values))) != 4:
            return False
        # We've exhausted all the error cases, so return true 
        return True

    def __eq__(self, subject):
        """
        __eq__

        Equivalence test; returns true when both values have the same
        self.addr value
        """
        if isinstance(subject, IPAddr):
            return self.addr == subject.addr
        elif isinstance(subject, int):
            return self.addr == int
        elif isinstance(subject, str):
            return IPAddr.int_ip_to_str(self.addr) == subject
        
    def __add__(self, subject):
        """
        __add__ (Self, subject)
        Creates a new IPAddr object with the sum of the integer
        address in the result.  Error management will be handled by the
        constructor.
        """
        if isinstance(subject, int):
            return IPAddr(self.addr + subject)
        elif isinstance(subject, IPAddr):
            return IPAddr(self.addr + subject.addr)
        
    def __str__(self):
        """
        String representation of the IP address; this will be in 
        dotted quad format.
        """
        return IPAddr.int_ip_to_str(self.addr)
    

    def __repr__(self):
        """
        Integer representation of address
        """
        return str(self.addr)

    
    def __init__(self, subject):
        """
        __init__(self, subject [int | str])
        Constructs the address object, storing the 
        subject in the internal value addr.  Handles integers
        or strings transparently.  
        """
        if isinstance(subject, str):
            if IPAddr.validate_str_address(subject):
                self.addr = IPAddr.str_ip_to_int(subject)
            else:
                raise Exception("Invalid String IP Address", subject)
        elif isinstance(subject, int):
            if IPAddr.validate_int_address(subject):
                self.addr = subject
            else:
                raise Exception("Invalid Integer IP Address", subject)
        else:
            raise Exception("Unknown Type of IP Address", subject)
        
def name_element(elt_cls, elt_epithet, elt_index):
    """
    name_element
    elt_cls: string
    elt_epithet: string
    elt_index: int, starting at 0 
    
    Returns a consistently formatted name for an element, the form
    is the epithet, the class and the index.  The epithet is a short
    term associated with everything in the subject network, the 
    class is the class (rtr, node), the index is a numerical index. 
    """
    return "%s_%s_%d" % (elt_epithet, elt_cls, elt_index)

def gen_subnet(testnet, hb_epithet, node_count, node_start_ip, subnet_prefix):
    """
    Creates a single subnetwork, which is to say a network which consists of
    one or more nodes sharing a common switch.  
    """
    nodes = [testnet.device(name_element('node', hb_epithet, x)) for x in range(0, node_count)]
    lan = testnet.connect(nodes)
    for index, node in enumerate(lan.endpoints):
        node.ip.addrs = ["%s/%d" % (str(node_start_ip + index), subnet_prefix)]
    return testnet
    
def gen_star(testnet, hb_epithet,
             node_count, node_start_ip,
             subnet_prefix):
    """
    Generates a star topology consisting of one central router and 
    n nodes in different subnetworks around the central router
    """
    main_router = testnet.device(name_element('rtr', hb_epithet, 0))
    # Generate node list
    nodes = [testnet.device(name_element('node', hb_epithet, x)) for x in
             range(0, node_count)]
    for index, i in enumerate(nodes):
        subject_network = testnet.connect([i, main_router])
        subject_network[i].ip.addrs = "%s/%d" % (str(node_start_ip + (2*index)),
                                                 subnet_prefix)
        subject_network[main_router].ip.addrs = "%s/%d" % (str(node_start_ip + 1 +
                                                     (2*index)),
                                                 subnet_prefix)
    return testnet

def constant_npr_gen(npr_val):
    #
    # The idea is that nodes_per_router should be a function, but at least
    # for the first case I don't want to have to spend my time working
    # with randomly generated stuff, so here's some currying
    return lambda x:npr_val

def gen_linear(testnet, hb_epithet,
               router_count, nodes_per_router,
               subnet_prefix, start_network,
               network_increment):
    """
    Generates a linear sequence of routers with subnets off of them. 
    """
    # Create the router devices
    routers = [
        testnet.device(name_element('rtr', hb_epithet, i))
        for i in range(0, router_count)
    ]
    # Now attach the individual nodes to each router
    for index, rtr in enumerate(routers):
        current_epithet = hb_epithet + '_%d' % index
        # First, create the various elements 
        current_router_elts = [
            testnet.device(name_element('node', current_epithet, j))
            for j in range(0, nodes_per_router(index))]
        current_router_network = testnet.connect(current_router_elts + [rtr])
        # Assign addresses
        for elt_index, elt in enumerate(current_router_elts):
            # Note the 2 here; it's important.
            # The 2 exists because each router is potentially connected
            # to a predecessor and successor.  The 2 reserves the
            # relative +0 and +1 for that purpose.
            current_address = str(start_network + \
                (network_increment * index) + 2 + elt_index)
            current_router_network[elt].ip.addrs = "%s/%d" % (current_address,
                                                              subnet_prefix)
    # At this point, we should have a collectin of routers with
    # elements dropping off of each one, now we interconnect the routers
    for index in range(0, len(routers) - 1):
        current_gate = testnet.connect([routers[index], routers[index + 1]])
        current_gate[routers[index]].ip.addrs = "%s/%d" % (
            str(start_network + (network_increment * index) + 1),
            subnet_prefix)
        current_gate[routers[index + 1]].ip.addrs = "%s/%d" % (
            str(start_network + (network_increment * (index + 1)) + 0),
            subnet_prefix)
    return testnet


def gen_ring(testnet, hb_epithet,
               router_count, nodes_per_router,
               subnet_prefix, start_network,
               network_increment):
    """
    A ring is a linear network where the first and last routers
    connect back on each other, this ensures that I have multiple
    paths to the same node and consequently have to determine what
    route is actually best from equivalently weighted options.
    """
    testnet = gen_linear(testnet, hb_epithet,
                         router_count, nodes_per_router,
                         subnet_prefix, start_network,
                         network_increment)
    # Now that the test network is created, we grab the first and
    # last routers.  Note that we're using the router names explicitly
    # to identify them.  
    first_index = len(testnet.devices) + 2
    last_index = -1
    first_device = None
    last_device = None
    for dev_index, dev_name in enumerate(testnet.devices):
        if dev_name.name.find('rtr_') > -1:
            if dev_index > last_index:
                last_index = dev_index
                last_device = dev_name
            if dev_index < first_index:
                first_index = dev_index
                first_device = dev_name
    new_subnet = tgt_network.connect([first_device, last_device])
    # I'm implicitly assuming that I've started with gen_linear
    # TODO: fix this so I'm not relying on that assumption
    new_subnet[first_device].ip.addrs = ["%s/%d" % (
        str(start_network + 0), subnet_prefix)]
    new_subnet[last_device].ip.addrs = ["%s/%d" % (
        str(start_network +
            ((last_index - first_index) * network_increment) + 1),
        subnet_prefix)]
    
    return testnet

def gen_complete(testnet, hb_epithet,
               router_count, nodes_per_router,
               subnet_prefix, start_network,
               network_increment):
    return testnet

# action = 'ring'
# tgt_network = mx.Topology('test')
# if action == 'star':
#     tgt_network = gen_star(tgt_network, 'test', 6, IPAddr('10.0.0.20'), 24)
# elif action == 'spine':
#     tgt_network = gen_linear(tgt_network, 'test', 4, constant_npr_gen(4),
#                              24, IPAddr('10.0.0.0'), 16)
# elif action == 'ring':
#     tgt_network = gen_ring(tgt_network, 'test', 4, constant_npr_gen(4),
#                            24, IPAddr('10.0.0.0'), 16)
# mx.experiment(tgt_network)

