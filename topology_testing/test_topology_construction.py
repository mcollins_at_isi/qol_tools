import base_topologies as bt
import mergexp as mx
import pytest

def test_simple_subnet_generation():
    """
    Tests that we create a subnet as expected
    """
    ntest_topo = mx.Topology('sample')
    total_nodes_created = 8 
    ntest_topo = bt.gen_subnet(ntest_topo, 'foo', total_nodes_created, bt.IPAddr('128.4.0.0'), '24')
    assert len(ntest_topo.nets) == 1
    assert len(ntest_topo.nets[0].endpoints) == total_nodes_created
    del ntest_topo

def test_subnet_address_generation():
    """
    Verifies that as a subnet is created, address assignmnet is correct.
    """
    test_topo = mx.Topology('test')
    total_nodes_created = 12
    initial_ip_address = bt.IPAddr('10.0.0.0')
    test_topo = bt.gen_subnet(test_topo, 'foo', total_nodes_created,
                              bt.IPAddr('10.0.0.0'), '24')
    active_nodes = test_topo.nets[0].endpoints
    for i in range(0, total_nodes_created):
        testnode = active_nodes[i]
        test_address = "%s/%s" % (initial_ip_address + i, '24')
        assert test_address == testnode.ip.addrs[0]
        
    

    
    
