#
#
# Basic test suite for IP address 
import base_topologies as bt
import pytest

def test_basic_correct_creation():
    # Sniff test -- can we generate from string and IP values
    subject = bt.IPAddr(22)
    assert subject.addr == 22
    subject = bt.IPAddr('0.0.0.29')
    assert subject.addr == 29

def test_complex_address():
    # Test the ability to create arbitrary addresses from strings.
    # need to test each octet
    subject = bt.IPAddr('0.0.0.88')
    assert subject.addr == 88
    subject = bt.IPAddr('0.0.200.0')
    assert subject.addr == 0x0000C800
    subject = bt.IPAddr('0.11.0.0')
    assert subject.addr == 0x000B0000
    subject = bt.IPAddr('100.0.0.0')
    assert subject.addr == 0x64000000
    subject = bt.IPAddr('1.255.0.2')
    assert subject.addr == 0x01FF0002
    subject = bt.IPAddr('255.255.255.255')
    assert subject.addr == 0xFFFFFFFF

def test_addr_errors():
    #
    # Verify that we force the exceptions
    with pytest.raises(Exception, match=r"Invalid Integer"):
        subject = bt.IPAddr(-3)
    with pytest.raises(Exception, match=r"Invalid Integer"):
        subject = bt.IPAddr(0xFFFFFFFF + 10)
    with pytest.raises(Exception, match=r"Invalid String"):
        subject = bt.IPAddr("foo")
    with pytest.raises(Exception, match=r"Invalid String"):
        subject = bt.IPAddr("99.2.299.3")
    with pytest.raises(Exception, match=r"Invalid String"):
        subject = bt.IPAddr("")
    with pytest.raises(Exception, match=r"Unknown Type"):
        subject = bt.IPAddr(None)
        
def test_addition():
    subject = bt.IPAddr("128.2.99.3")
    result = bt.IPAddr("128.2.99.0")
    assert subject == (result + 3)
    assert subject == result + bt.IPAddr("0.0.0.3")
    subject = bt.IPAddr("128.2.1.3")
    result = bt.IPAddr("128.2.0.3")
    assert subject == result + 256

def test_addr_alibi():
    #
    # Anything that doesn't fit anywhere else
    #
    subject = bt.IPAddr(0)
    assert subject.addr == 0 
