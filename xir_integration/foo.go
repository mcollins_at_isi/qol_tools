package main
// First attempt at a real go program, this is going to be laughably
// Primitive
import "fmt"
import "os"
import "gitlab.com/mergetb/xir/lang/go"
import "gitlab.com/mergetb/engine/pkg/realize"
//import "gitlab.com/mergetb/xir/lang/go/xp"
func get_ips(ep xir.Endpoints) []string {
	//
	// Pulls an array of IP addresses from an endpoints array
	result := make([] string, 0)
	for _,k := range ep {
		//		fmt.Println(k.Props)
		if value, check := k.Props.GetProps("ip") ; check {
			//fmt.Println("Found props as an object")
			//fmt.Println(value)
			if address, check := value["addrs"]; check {
				fmt.Println("Found address array")
				foo := address.([]interface{})
				for _,addr := range foo {
					result = append(result, addr.(string))
					//					fmt.Println("ip: ", addr.(string))
					
				}
			}
		}
	}
	return result
}
func main() {
	var source_file string
	var exit_file string
	clargs := os.Args
	
	if len(clargs) < 3 {
		fmt.Println("Incorrect args")
		os.Exit(-1)
	} else {
		source_file = clargs[1]
		exit_file = clargs[2]
	}
	fmt.Println("Going from ", source_file, " to",  exit_file)
	net, err := xir.FromFile(source_file)
	if err != nil {
		fmt.Println("Error reading XIR file", source_file, ":", err)
		os.Exit(-1)
	}
	fmt.Println(net);
	df := realize.DijkstraForest(net)
	// Next step, generate a table of each node.
	for k,v := range df {
		props := net.GetElementProps(k.Id)
		addrs := get_ips(props.EndPoints)
		for _,w := range(addrs) {
			fmt.Println(w)
		}
	}
}
