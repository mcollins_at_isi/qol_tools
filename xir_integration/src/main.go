package main
// First attempt at a real go program, this is going to be laughably
// Primitive
import "fmt"
import "os"
import "gitlab.com/mergetb/xir/lang/go"
import "gitlab.com/mergetb/engine/pkg/realize"


func DoBuildRoutes(net *xir.Net, df map[*xir.Node](*realize.DijkstraState)) {
	//DoBuildRoutes -- master routing routine
	//
	// DoBuildRoutes goes through the network object, generates a set of shortest paths and
	// then annotates nodes with routing information so that every node in the graph can be
	// reached by every connected node. 
	
	var full_name_list []string

	full_name_list = make([]string, 0)

	//	df := realize.DijkstraForest(net)
	for n,_ := range df {
		full_name_list = append(full_name_list, n.Label())
	}
	//
	// This is an APSP algorithm, so what it does is iterate through ever combination of endpoints.
	for _,start_node := range net.Nodes {
		for _,end_node := range net.Nodes {
			// The conditional here skips when the node would point to itself.  We're
			// still doing A-B and B-A though.  Asymmetric routes are a thing, kids!
			// Well, they'd be a thing if we accounted for path weights.
			if start_node.Label() != end_node.Label() {
				// The checks here mean that we're not considering routers
				// as a start/endpoint for a route.
				if (len(start_node.Endpoints) > 1) {
					break
				} 
				if len(end_node.Endpoints) <= 1 {
					// So, we've reached a set of candidate endpoints -- both
					// start and endpoint are singlehomed, we generate a shortest
					// path and then build the route across it.
					sp, ok := realize.ShortestPath(
						start_node, end_node, df)
					if ok { 
						GenerateRoutesFromPath(start_node, end_node, sp)
					}
				}
			}
		}
	}
}

func main() {
	var source_file, dest_file string
	clargs := os.Args
	
	//
	// This application is implemted as a reticulator, so it takes in a file and outputs a
	// file.  It expects to have those two arguments and nothing else.
	//
	
	if len(clargs) <= 2 {
		fmt.Println("Incorrect arg count")
		os.Exit(-1)
	} else {
		source_file = clargs[1]
		dest_file = clargs[2]
	}
	net, err := xir.FromFile(source_file)
	df := realize.DijkstraForest(net)
	if err != nil {
		os.Exit(-1)
	}
	DoBuildRoutes(net, df)
	err = net.ToFile(dest_file)
}
