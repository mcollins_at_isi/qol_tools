package main

import (
	"gitlab.com/mergetb/xir/lang/go"
	"net"
	"reflect"
	//import "fmt"
	"strings"
)

func FindLongestCommonPrefix(ip []net.IP) (result int, ok bool) {
	//
	// Returns the longest common netmask for an arbitary array
	// of IP addresses
	//
	ok = false
	for result = 32; result > 1; result-- {
		broke := false
		origin := ip[0].Mask(net.CIDRMask(result, 32))
		for _, v := range ip {
			masked_v := v.Mask(net.CIDRMask(result, 32))
			if !masked_v.Equal(origin) {
				broke = true
			}
		}
		// We didn't break internally, so this is
		// the longest netmask where everything worked
		if !broke {
			break
		} else {
			broke = false
		}
	}
	ok = true
	return
}

func GetIPsFromEndpoints(ep xir.Endpoints) []string {
	//
	// Pulls an array of IP addresses from an endpoints array
	result := make([]string, 0)
	for _, k := range ep {
		if value, check := k.Props.GetProps("ip"); check {
			if address, check := value["addrs"]; check {
				switch v := reflect.ValueOf(address); v.Kind() {
				case reflect.String:
					{
						offset_index := strings.Index(address.(string), "/")
						if offset_index == -1 {
							result = append(result,
								address.(string))
						} else {
							result = append(result,
								address.(string)[:offset_index])
						}
					}
				default:
					{
						foo := address.([]interface{})
						for _, addr := range foo {
							result = append(result, addr.(string))
						}
					}
				}
			}
		}
	}
	return result
}

func FindGateways(net *xir.Net) []string {
	var result []string
	result = make([]string, 0)
	gateway_table := make(map[string]int)
	for _, k := range net.Links {
		for _, l := range k.Endpoints {
			index := l.Parent.Label()
			if gateway_table[index] == 0 {
				gateway_table[index] = 1
			} else {
				gateway_table[index] += 1
			}
		}
	}
	for key, value := range gateway_table {
		if value > 1 {
			result = append(result, key)
		}
	}
	return result

}

func GuessGatewayAddress(src_node, next_hop, end_node *xir.Node) (
	result string, ok bool) {
	// guess_gw guesses the gateway ip address that src_node takes
	// to get to dst_ip via the next_hop node.  The assumption here
	// is that the next_hop node is multihomed and we have to pick the
	// interface which has the shortest prefix
	prefix := 0
	var stash_dst string
	var address_found bool
	src_ips := GetIPsFromEndpoints(src_node.Endpoints)
	hop_ips := GetIPsFromEndpoints(next_hop.Endpoints)
	final_string := GetIPsFromEndpoints(end_node.Endpoints)
	// At this point, search through the set of IP's and find the one that
	// has the tightest subnetwork match between the two.  That's the routing guess
	// And I should really look at xir linkages to limit the set.
	for _, i := range src_ips {
		src_ip_int := net.ParseIP(i)
		for _, j := range hop_ips {
			dst_ip_int := net.ParseIP(j)
			v, _ := FindLongestCommonPrefix([]net.IP{src_ip_int, dst_ip_int})
			if v > prefix {
				prefix = v
				stash_dst = j
			}
		}
	}
	//if !CheckForNetblockMatch(final_string[0], stash_dst) {

	// Check to make sure that the tightest match isn't just the same address.

	if final_string[0] != stash_dst {
		// "Empty" case, see if route isn't available
		address_found = false
		if _, ok := src_node.Props["route"]; !ok {
			src_node.Props["route"] = []xir.Props{}
		} else {
			// Check to see whether or not a route to the
			// destination exists
			for _, elt := range src_node.Props["route"].([]xir.Props) {
				if elt["dst"] == final_string[0] {
					address_found = true
				}
			}
		}
		if !address_found {
			np := xir.Props{"dst": final_string[0] + "/32", "gw": stash_dst}
			src_node.Props["route"] = append(src_node.Props["route"].([]xir.Props), np)
		}
	}
	result = stash_dst
	ok = true
	return
}

func GenerateRoutesFromPath(start_node, end_node *xir.Node, path []*xir.Link) (ok bool) {
	//
	// Given a shortest path, this routine will generate the route
	// by walking backwards from the endpoint
	node_path := make([]*xir.Node, 0)
	ok = false
	node_path = append(node_path, start_node)
	if len(path) == 1 {
		// This is a no-op
		ok = true
	} else {
		var index int
		for index = 0; index < (len(path) - 1); index++ {
			current_hop := path[index]
			next_hop := path[index+1]
			nodes_in_link := make(map[string]int)
			for _, k := range current_hop.Endpoints {
				nodes_in_link[k.Parent.Label()] = 1
			}
			for _, k := range next_hop.Endpoints {
				if nodes_in_link[k.Parent.Label()] == 1 {
					node_path = append(node_path, k.Parent)
					break
				}
			}
		}
	}
	node_path = append(node_path, end_node)

	name_path := make([]string, 0)
	for _, k := range node_path {
		name_path = append(name_path, k.Label())
	}
	// At this point, we have the complete sequence of nodes,
	// now we can construct the appropriate
	// gateway instructions
	if len(node_path) <= 1 {
		ok = true
		return
	}
	for i := 0; i < len(node_path)-1; i++ {
		GuessGatewayAddress(node_path[i], node_path[i+1], end_node)
	}
	ok = true
	return
}
