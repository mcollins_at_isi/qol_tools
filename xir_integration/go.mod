module gitlab.com/mergetb/xir/lang/go/v0.1.8

go 1.13

require (
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.5.0 // indirect
	gitlab.com/mergetb/engine v0.2.7
	gitlab.com/mergetb/xir v0.1.8
)
