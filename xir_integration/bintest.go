package main
import "fmt"

func main() {
	s := 0xFFFFFFFF
	
	fmt.Printf("%08X\n", s)
	fmt.Printf("%08X\n", s << 4)
	fmt.Printf("%08X\n", (s << 4) & 0xFFFFFFFF)
}
